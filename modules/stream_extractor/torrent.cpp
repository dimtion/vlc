/*****************************************************************************
 * torrent.cpp: torrent client
 *****************************************************************************
 * Copyright (C) 2017-2017 VLC authors and VideoLAN
 * $Id$
 *
 * Authors: Filip Roséen <filip@atch.se>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <vlc_common.h>
#include <vlc_plugin.h>
#include <vlc_stream.h>
#include <vlc_playlist.h>
#include <vlc_interface.h>
#include <vlc_input_item.h>
#include <vlc_stream_extractor.h>
#include <vlc_access.h>
#include <vlc_fs.h>

#include <libtorrent/entry.hpp>
#include <libtorrent/bencode.hpp>
#include <libtorrent/session.hpp>
#include <libtorrent/ip_filter.hpp>
#include <libtorrent/magnet_uri.hpp>
#include <libtorrent/alert_types.hpp>
#include <libtorrent/torrent_info.hpp>
#include <libtorrent/torrent_handle.hpp>
#include <libtorrent/create_torrent.hpp>

#include <map>
#include <thread>
#include <future>
#include <memory>
#include <vector>
#include <string>
#include <atomic>
#include <fstream>
#include <cstdlib>
#include <cstring>
#include <iterator>

#define BLOCKLIST_TEXT N_("path to blocklist file")
#define DOWNLOAD_PATH_TEXT N_("Dowload folder")
#define RETAIN_TEXT N_("Keep downloaded files")
#define BACKGROUND_TEXT N_("Download in background, *not implemented yet*")

extern "C" int DirectoryOpen( vlc_object_t* opaque );
extern "C" void DirectoryClose( vlc_object_t* opaque );
extern "C" int ExtractorOpen( vlc_object_t* opaque );
extern "C" void ExtractorClose( vlc_object_t* opaque );
extern "C" int MagnetOpen( vlc_object_t* opaque );
extern "C" int InterfaceOpen( vlc_object_t* opaque );
extern "C" void InterfaceClose( vlc_object_t* opaque );

vlc_module_begin()
    set_shortname( "torrent" )
    set_description( N_( "libtorrent based stream directory" ) )
    set_category( CAT_INPUT )
    set_subcategory( SUBCAT_INPUT_STREAM_FILTER )
    set_capability( "stream_directory", 99 )
    set_callbacks( DirectoryOpen, DirectoryClose );
    add_submodule()
        set_description( N_( "libtorrent based stream extractor" ) )
        set_capability( "stream_extractor", 99 )
        add_shortcut( "file" )
        set_callbacks( ExtractorOpen, ExtractorClose )
    add_submodule()
        set_description( N_( "magnet-redirector" ) )
        set_capability( "access", 99 )
        add_shortcut( "file" )
        set_callbacks( MagnetOpen, NULL )
    add_submodule()
        set_description( N_( "torrent interface" ) )
        set_capability( "interface", 99 )
        add_string( "torrent-blocklist", NULL, BLOCKLIST_TEXT, BLOCKLIST_TEXT, false )
        add_string( "torrent-download-path", NULL, DOWNLOAD_PATH_TEXT, DOWNLOAD_PATH_TEXT, false )
        add_bool( "torrent-retain-downloads", true, RETAIN_TEXT, RETAIN_TEXT, false )
        add_bool( "torrent-background-downloads", false, BACKGROUND_TEXT, BACKGROUND_TEXT, false )
        set_callbacks( InterfaceOpen, InterfaceClose )
vlc_module_end()

namespace lt = libtorrent;

struct btp_client {
    btp_client( vlc_object_t* owner )
        : keep_alive( true )
        , owner( owner )
    {
        char* psz_blocklist = var_InheritString( owner, "torrent-blocklist" );

        if ( psz_blocklist != NULL )
        {
            stream_t* filter_stream = vlc_stream_NewURL( owner, psz_blocklist );
            uint64_t filter_count = 0;
            lt::ip_filter filter;

            if( !filter_stream )
                throw std::runtime_error( "Unable to open specified blocklist" );

            while( char* line = vlc_stream_ReadLine( filter_stream ) )
            {
                std::istringstream iss( line );
                std::string start, end, desc;

                if( getline( iss, desc, ':' ) &&
                    getline( iss, start, '-' ) &&
                    getline( iss, end ) )
                {
                    try
                    {
                        filter.add_rule( lt::address::from_string( start ),
                                         lt::address::from_string( end ),
                                         lt::ip_filter::blocked );
                        ++filter_count;
                    }
                    catch( ... )
                    {
                        /* intentionally left blank */
                    }
                }

                free( line );
            }

            vlc_stream_Delete( filter_stream );

            session.set_ip_filter( filter );
            msg_Dbg( owner, "added %" PRIu64 " entries to the blocklist", filter_count );
        }
        lt::settings_pack settings;
        settings.set_int( lt::settings_pack::alert_mask,
                          lt::alert::storage_notification
                          | lt::alert::status_notification
                          | lt::alert::error_notification );

        session.apply_settings( settings );
        worker = std::thread( &btp_client::thread, this );
    }

    template<class Func>
    void sched( Func&& func )
    {
        std::lock_guard<std::mutex> sync { lock };
        pending_work.push_back( std::forward<Func>( func ) );
    }

    template<class Alert, class Func, int Alert_Id = Alert::alert_type>
    auto probe( Func func ) -> std::unique_ptr<std::promise<decltype( func( lt::alert_cast<Alert>( std::declval<lt::alert*>() ) ).second )>>
    {
        auto promise = new std::promise<decltype( func( lt::alert_cast<Alert>( std::declval<lt::alert*>() ) ).second )> {};
        auto promise_ptr = std::unique_ptr<typename std::remove_reference<decltype(*promise)>::type>( promise );

        auto wrapper = [=]( lt::alert* alert ) {
            if( !alert )
            {
                fprintf( stderr, "setting exception on thing" );
                promise->set_exception( std::make_exception_ptr( std::logic_error( "canceled" ) ) );
                return false;
            }

            auto value = func( lt::alert_cast<Alert>( alert ) );

            if( value.first )
                promise->set_value( value.second );

            return value.first;
        };

        { std::lock_guard<std::mutex> sync { lock };
          pending_probes.emplace( Alert_Id, wrapper ); }

        return std::move( promise_ptr );
    }


    void thread()
    {
        while( keep_alive.load() )
        {
            std::vector<lt::alert*> alerts;

            decltype( pending_work ) work;
            decltype( pending_probes ) probes;

            { std::lock_guard<std::mutex> sync { lock };
              std::swap( work, pending_work ); }

            for( auto&& worker : work )
                worker();

            if( !session.wait_for_alert( lt::seconds( 1 ) ) )
                continue;

            session.pop_alerts( &alerts );

            { std::lock_guard<std::mutex> sync { lock };
              std::swap( probes, pending_probes ); }

            for( auto a : alerts )
            {
                msg_Dbg( owner, "libtorent alert: %s", a->message().c_str() );
                auto matches = probes.equal_range( a->type() );

                for( auto it = matches.first; it != matches.second; )
                    it = it->second( a ) ? probes.erase( it ) : next( it );
            }

            { std::lock_guard<std::mutex> sync { lock };
              pending_probes.insert( std::make_move_iterator( probes.begin() ),
                                     std::make_move_iterator( probes.end() ) ); }
        }
    }

    ~btp_client()
    {
        keep_alive.store( false );
        if( worker.joinable() )
            worker.join();

        for( auto& probe : pending_probes )
            probe.second ( nullptr );
    }

    std::mutex lock;
    std::thread worker;
    lt::session session;
    std::atomic_bool keep_alive;
    std::vector<std::function<void()>> pending_work;
    std::multimap<int, std::function<bool(lt::alert*)>> pending_probes;
    std::map<lt::sha1_hash, std::weak_ptr<struct btp_torrent>> torrents;

    vlc_object_t* owner;
};

struct btp_torrent {
    btp_torrent( btp_client& client ) : client( client ) { }

    ~btp_torrent()
    {
        auto client = &this->client;
        auto handle =  this->handle;
        client->sched( [=]{ handle.pause( handle.graceful_pause ); } );
    }

    btp_client& client;
    lt::torrent_handle handle;
};

struct btp_entry : lt::file_storage {
    btp_entry( int file_descriptor, lt::file_storage base,
               btp_client* client, std::shared_ptr<btp_torrent> torrent
      ) : lt::file_storage( base )
        , file_descriptor( file_descriptor )
        , client( client )
        , torrent( torrent )
    { }

    int const file_descriptor;
    uint64_t read_offset = 0;
    uint64_t const size = file_size( file_descriptor );
    std::string const path = file_path( file_descriptor );

    btp_client* client;
    std::shared_ptr<btp_torrent> torrent;
};

std::shared_ptr<btp_torrent>
torrent_handle_for( btp_client* client, lt::torrent_info const& ti, lt::add_torrent_params* ptr_params )
{
    auto it = client->torrents.find( ti.info_hash() );
    std::shared_ptr<btp_torrent> handle;

    if( it != client->torrents.end() )
        handle = it->second.lock();

    if( !handle )
    {
        lt::add_torrent_params params;

        if( ptr_params == nullptr )
        {
            char* psz_path;
            asprintf( &psz_path, "%s" DIR_SEP "torrents", config_GetUserDir(VLC_CACHE_DIR));
            params.save_path = strdupa(psz_path);
            params.flags |= lt::add_torrent_params::flag_paused;
            params.ti.reset( new lt::torrent_info( ti ) );
            free( psz_path );
        }
        else
            params = *ptr_params;

        auto handle_promise = std::promise<std::shared_ptr<btp_torrent>> {};

        client->sched( [&]
        {
            auto error = lt::error_code {};
            auto result = std::make_shared<btp_torrent>( *client );
            result->handle = client->session.add_torrent( params, error );

            if( error )
                result.reset();

            handle_promise.set_value( result );
        });

        if( handle = handle_promise.get_future().get() )
            client->torrents.emplace( ti.info_hash(), handle );
    }

    return handle;
}

/**
 * Create a vector of torrent files using the informations in `data`
 *
 * \param client bitorrent client to use
 * \param data a valid bitorrent file
 * \return list of files of the torrent
 */
std::vector<btp_entry>
TorrentCreate( btp_client* client, std::string data )
{
    auto  error = lt::error_code {};
    auto   info = lt::torrent_info{ data.c_str(), (int)data.size(), error };

    if( error )
        return {};

    auto torrent = torrent_handle_for( client, info, nullptr );
    auto entries = std::vector<btp_entry> {};

    if( torrent )
    {
        for( int i = 0; i < info.files().num_files(); ++i )
            entries.push_back( btp_entry{ i, info.files(), client, torrent } );
    }

    return entries;
}

/**
 * Setup download folder
 *
 * \param intf interface pointer
 * \return 0 if ok, -1 if error
 */
int
SetupDirectory( intf_thread_t* intf )
{
    char *psz_dir;
    char *psz_cachedir = config_GetUserDir(VLC_CACHE_DIR);
    asprintf( &psz_dir, "%s" DIR_SEP "torrents", psz_cachedir );

    bool clean = !var_InheritBool( intf, "torrent-retain-downloads" );
    struct stat st;

    int res;

    if ( vlc_stat( psz_dir, &st ) == -1)
    {
        msg_Dbg( intf, "creating torrent folder" );
        res = vlc_mkdir( psz_dir, 0700 );
    }
    else if ( clean )
    {
        msg_Dbg( intf, "recreating torrent folder" );
        res = remove( psz_dir );
        if ( res == 0 )
            res = vlc_mkdir( psz_dir, 0700 );
    }

    free( psz_dir );
    free( psz_cachedir );
    return res;
}

extern "C" int InterfaceOpen( vlc_object_t* obj ) try
{
    auto intf = reinterpret_cast<intf_thread_t*>( obj );
    auto client = std::unique_ptr<btp_client>( new btp_client( obj ) );

    if( var_Create( pl_Get( intf ), "torrent-client",
          VLC_VAR_ADDRESS | VLC_VAR_DOINHERIT ) )
    {
        throw std::runtime_error( "variable creation failed" );
    }

    if( var_SetAddress( pl_Get( intf ), "torrent-client", client.get() ) )
        throw std::runtime_error( "variable assignment failed" );

    // Create necessary folders
    SetupDirectory( intf );

    intf->p_sys = reinterpret_cast<intf_sys_t*>( client.release() );
    return VLC_SUCCESS;
}
catch( std::exception const& e )
{
    msg_Err( obj, "%s", e.what() );
    return VLC_EGENERIC;
}

extern "C" void InterfaceClose( vlc_object_t* opaque )
{
    auto intf = reinterpret_cast<intf_thread_t*>( opaque );
    SetupDirectory( intf );
    delete reinterpret_cast<btp_client*>( intf->p_sys );
}

extern "C" int MagnetOpen( vlc_object_t* obj ) try
{
    stream_t* access = (stream_t*)obj;

    if( access->b_preparsing || access->psz_filepath == NULL )
        return VLC_EGENERIC;

    char* filename = strrchr( access->psz_filepath, '/' );

    if( filename == NULL || strncasecmp( filename+1, "magnet:?", 8 ) )
        return VLC_EGENERIC; /* not a magnet */

    auto client = reinterpret_cast<btp_client*>(
        var_InheritAddress( access, "torrent-client" ) );

    char* psz_torrent;
    char* psz_dir;
    char* psz_cachedir = config_GetUserDir(VLC_CACHE_DIR);
    asprintf( &psz_dir, "%s" DIR_SEP "torrents", psz_cachedir );
    asprintf( &psz_torrent, "%s" DIR_SEP "current.torrent", psz_dir );

    char* custom_dir = var_InheritString( access, "torrent-download-path" );
    if ( custom_dir != NULL ) {
            psz_dir = strdup( custom_dir );
    }
    free( custom_dir );

    if( client == NULL )
        throw std::runtime_error( "torrent client is not loaded" );

    {
        lt::error_code error;
        lt::torrent_handle torrent;
        lt::add_torrent_params params;
        lt::parse_magnet_uri( filename + 1, params, error );

        params.save_path = strdupa(psz_dir);
        params.flags |= lt::add_torrent_params::flag_paused;

        if( error )
            throw std::runtime_error( "unable to parse magnet-uri" );

        auto metadata = std::promise<std::string>{};

        msg_Dbg( access, "subscribing to metadata alert" );

        auto buffer = client->probe<lt::metadata_received_alert>( [&]( auto alert )
        {
            std::string buffer;

            if( torrent != alert->handle )
                return std::make_pair( false, buffer );

            msg_Dbg( access, "generating torrent data" );

            lt::bencode( back_inserter( buffer ),
                lt::create_torrent( *alert->handle.torrent_file() ).generate() );

            return std::make_pair( true, buffer );
        });

        client->sched( [&]
        {
            msg_Dbg( access, "adding magnet for metadata subscription" );

            lt::error_code error;
            torrent = client->session.add_torrent( params, error );

            if( error )
            {
                msg_Err( access, "unable to add torrent for generating metadata" );
                return;
            }
        });

        msg_Dbg( access, "client->session.add_torrent was successful" );

        std::ofstream( psz_torrent, std::ios::binary ) << buffer->get_future().get();
    }

    asprintf( &psz_torrent, "file://" DIR_SEP "%s", psz_torrent );
    access->psz_url = strdup( psz_torrent );

    free( psz_cachedir );
    free( psz_dir );
    free( psz_torrent );
    return VLC_ACCESS_REDIRECT;
}
catch( std::exception const& e )
{
    msg_Err( obj, "MagnetOpen: %s", e.what() );
    return VLC_EGENERIC;
}

extern "C" int SEReadDir( stream_directory_t* directory, input_item_node_t* parent )
{
    auto& entries = *static_cast<std::vector<btp_entry>*>( directory->p_sys );

    for( auto& entry : entries )
    {
        char* mrl = vlc_stream_extractor_CreateMRL( directory, entry.path.c_str() );

        if( unlikely( !mrl ) )
            continue;

        input_item_t* item = input_item_New( mrl, entry.path.c_str() );

        if( likely( item ) )
        {
            input_item_CopyOptions( parent->p_item, item );
            input_item_node_AppendItem( parent, item );
            input_item_Release( item );
        }

        free( mrl );
    }

    return VLC_SUCCESS;
}

extern "C" block_t* SEBlock( stream_extractor_t* extractor, bool* eof )
{
    auto entry = reinterpret_cast<btp_entry*>( extractor->p_sys );

    if( entry->read_offset >= entry->size )
    {
        *eof = true;
        return NULL;
    }

    auto& torrent = entry->torrent->handle;
    auto request = entry->map_file( entry->file_descriptor, entry->read_offset, 1 );
    auto block = block_Alloc( entry->piece_size( entry->file_descriptor ) );

    if( unlikely( !block ) )
    {
        msg_Err( extractor, "failed to allocate memory for block" );
        return NULL;
    }

    msg_Dbg( extractor, "scheduling request for block{ fd: %d, idx: %d }",
        entry->file_descriptor, request.piece );

    auto _block_size = entry->client->probe<lt::read_piece_alert>( [&]( auto alert )
    {
        if( alert == NULL )
            return std::make_pair( false, int{} );

        if( alert->piece != request.piece || alert->handle != torrent )
            return std::make_pair( false, int{} );

        if( !alert->buffer )
        {
            torrent.read_piece( request.piece );
            return std::make_pair( false, int{} );
        }

        std::copy( alert->buffer.get() + request.start,
                   alert->buffer.get() + alert->size,
                   block->p_buffer );

        return std::make_pair( true, int( alert->size - request.start ) );
    });

    entry->client->sched( [&]
    {
        torrent.set_piece_deadline( request.piece, 0,
                                    torrent.alert_when_available );

    });

    block->i_buffer = _block_size->get_future().get();
    entry->read_offset += block->i_buffer;

    msg_Dbg( extractor, "received block{ fd: %d, idx: %d }",
        entry->file_descriptor, request.piece );

    return block;
}

extern "C" int SESeek( stream_extractor_t* extractor, uint64_t offset )
{
    static_cast<btp_entry*>( extractor->p_sys )->read_offset = offset;
    return VLC_SUCCESS;
}

extern "C" int SEControl( stream_extractor_t* extractor, int req, va_list args )
{
    auto entry = static_cast<btp_entry*>( extractor->p_sys );

    switch( req )
    {
        case STREAM_CAN_SEEK:
        case STREAM_CAN_PAUSE:
        case STREAM_CAN_CONTROL_PACE: *va_arg (args, bool *) = true; break;
        case STREAM_IS_DIRECTORY: return VLC_EGENERIC;
        case STREAM_GET_SIZE: *va_arg(args, uint64_t *) = entry->size; break;
        case STREAM_CAN_FASTSEEK:
                              *va_arg (args, bool *) = false; break;
        case STREAM_GET_PTS_DELAY:
                              *va_arg( args, int64_t*) = DEFAULT_PTS_DELAY; break;
        default:
            return vlc_stream_vaControl( extractor->source, req, args );
    }

    return VLC_SUCCESS;
}

extern "C" int DirectoryOpen( vlc_object_t* opaque ) try
{
   auto directory = reinterpret_cast<stream_directory_t*>( opaque );

   char const magic_bytes[] = {'d','8',':','a','n','n','o','u','n','c','e' };
   ssize_t magic_size = sizeof( magic_bytes );

   uint8_t const* initial;
   if( vlc_stream_Peek( directory->source, &initial, magic_size ) < magic_size )
       return VLC_EGENERIC;

    if( memcmp( initial, magic_bytes, sizeof( magic_bytes ) ) )
        return VLC_EGENERIC;

    std::ostringstream oss;

    while( !vlc_stream_Eof( directory->source ) )
    {
        char buffer[256];
        int n = vlc_stream_Read( directory->source, buffer, sizeof( buffer ) );

        if( n < 0 )
            break;

        oss.write( buffer, n );
    }

    btp_client* client = reinterpret_cast<btp_client*>(
        var_InheritAddress( directory, "torrent-client" ) );

    if( client == NULL )
        throw std::runtime_error( "missing torrent client" );

    auto entries = TorrentCreate( client, oss.str() );

    directory->pf_readdir = SEReadDir;
    directory->p_sys = static_cast<void*>( new std::vector<btp_entry>( entries ) );

    return VLC_SUCCESS;

}
catch( std::exception const& e )
{
    msg_Err( opaque, "open failed: %s", e.what() );
    return VLC_EGENERIC;
}

extern "C" void DirectoryClose( vlc_object_t* opaque )
{
    auto directory = reinterpret_cast<stream_directory_t*>( opaque );
    delete static_cast<std::vector<btp_entry>*>( directory->p_sys );
}

extern "C" int ExtractorOpen( vlc_object_t* opaque ) try
{
    auto extractor = reinterpret_cast<stream_extractor_t*>( opaque );

    char const magic_bytes[] = {'d','8',':','a','n','n','o','u','n','c','e' };
    ssize_t magic_size = sizeof( magic_bytes );

    uint8_t const* initial;
    if( vlc_stream_Peek( extractor->source, &initial, magic_size ) < magic_size )
        return VLC_EGENERIC;

    if( memcmp( initial, magic_bytes, sizeof( magic_bytes ) ) )
        return VLC_EGENERIC;

    std::ostringstream oss;

    while( !vlc_stream_Eof( extractor->source ) )
    {
        char buffer[256];
        int n = vlc_stream_Read( extractor->source, buffer, sizeof( buffer ) );

        if( n < 0 )
            break;

        oss.write( buffer, n );
    }

    btp_client* client = reinterpret_cast<btp_client*>(
        var_InheritAddress( extractor, "torrent-client" ) );

    if( client == NULL )
        throw std::runtime_error( "missing torrent client" );

    auto entries = TorrentCreate( client, oss.str() );

    auto it = std::find_if( entries.begin(), entries.end(),
        [&]( auto&& entry ) { return entry.path == extractor->identifier; } );

    if( it == entries.end() )
        return VLC_EGENERIC;

    btp_entry* entry = new btp_entry( *it );

    extractor->pf_block = SEBlock;
    extractor->pf_control = SEControl;
    extractor->pf_seek = SESeek;
    extractor->p_sys = static_cast<void*>( entry );

    client->sched( [=] { entry->torrent->handle.resume(); } );

    return VLC_SUCCESS;
}
catch( std::exception const& e )
{
    msg_Err( opaque, "open failed: %s", e.what() );
    return VLC_EGENERIC;
}

extern "C" void ExtractorClose( vlc_object_t* opaque )
{
    auto extractor = reinterpret_cast<stream_extractor_t*>( opaque );
    delete static_cast<btp_entry*>( extractor->p_sys );
}


